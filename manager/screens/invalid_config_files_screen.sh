# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_invalid_config_files_screen_sh_included}" ]] && _fdm_invalid_config_files_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/run.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/validation.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/display.sh"

function fdm::invalid_config_files_screen::refresh_data() {
    invalid_config_files_screen_data__error=$(docker::check_service "${FDM_SERVICE}")
    invalid_config_files_screen_data__file=$(docker::service_properties_file "${FDM_SERVICE}")
}

function fdm::invalid_config_files_screen::header() {
    if [[ -n "${invalid_config_files_screen_data__error}" ]]; then
        echo "Your changes have invalidated the service!"
        fdm::theme::invalid "${invalid_config_files_screen_data__error}"; echo
    fi
}

function fdm::invalid_config_files_screen::menu() {
    if [[ -n "${invalid_config_files_screen_data__error}" ]]; then
        echo "You will now be taken back to the properties file to fix the problem."
        echo
        prompt::press_to_continue
        fdm::run::file_editor "${invalid_config_files_screen_data__file}"
        fdm::display::loading
        if [[ -z "$(docker::check_service "${FDM_SERVICE}")" ]]; then
            # shellcheck disable=SC2034
            FDM_SCREEN='config_files_screen'
        fi
    fi
}

function fdm::invalid_config_files_screen::validate() {
    fdm::validation::check_conditions_for_invalid_config_files_screen

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        # shellcheck disable=SC2034
        FDM_SCREEN='service_screen'
        return 1
    fi

    return 0
}

