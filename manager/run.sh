# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_run_sh_included}" ]] && _fdm_run_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"

function fdm::run::script() {
    util::mandatory_min_arg_count 2 "$@"
    local readonly _validation_function="$1"
    local readonly _script="$2"
    local readonly _args="${*:3}"

    eval "${_validation_function}" || return 1

    docker::run_script "${_script}" "${_args}"

    echo
    fdm::message::logs_can_be_found_here
    echo
    prompt::press_to_continue
}

function fdm::run::confirmed_script() {
    util::mandatory_min_arg_count 2 "$@"
    local readonly _validation_function="$1"
    local readonly _script="$2"
    local readonly _args="${*:3}"

    prompt::request_confirmation_Yn 'Do you want to continue?' || return 1

    fdm::run::script "${_validation_function}" "${_script}" "${_args}"
}

function fdm::run::function() {
    util::mandatory_min_arg_count 2 "$@"
    local readonly _validation_function="$1"
    local readonly _function_to_run="$2"
    local readonly _args="${*:3}"

    eval "${_validation_function}" || return 1

    eval "${_function_to_run} ${_args}"
    local readonly _status="$?"
    
    echo
    prompt::press_to_continue

    return "${_status}"
}

function fdm::run::confirmed_function() {
    util::mandatory_min_arg_count 2 "$@"
    local readonly _validation_function="$1"
    local readonly _function_to_run="$2"
    local readonly _args="${*:3}"

    prompt::request_confirmation_Yn 'Do you want to continue?' || return 1

    fdm::run::function "${_validation_function}" "${_function_to_run}" "${_args}"
}

function fdm::run::file_editor {
    util::mandatory_arg_count 1 "$@"
    local readony _file="$1"

    # we start the editor in a subshell to trap the EXIT signal in order to release the lock
    (
        docker::lock;
        if [[ $? -ne 0 ]]; then prompt::press_to_continue; exit 1; fi;
        trap 'docker::unlock; display::hide_cursor' EXIT
        ${FRIENDLY_DOCKER_EDITOR} "${_file}"
    )
}

