# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_backup_screen_sh_included}" ]] && _fdm_backup_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/run.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/validation.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/properties.sh"

function fdm::backup_screen::refresh_data() {
    _fdm::backup_screen::init_data
    _fdm::backup_screen::refresh_backup_data
    _fdm::backup_screen::refresh_action_list
}

function _fdm::backup_screen::init_data() {
    backup_screen_data__options=()
}

function _fdm::backup_screen::refresh_backup_data() {
    if docker::is_persistent_backup "${FDM_BACKUP}"; then
        backup_screen_data__is_persistent=0
    else
        backup_screen_data__is_persistent=1
    fi

    backup_screen_data__totalsize=$(util::totalsize_human_readable "${FDM_BACKUP}")
    backup_screen_data__description=$(text::sanitize_and_untabify "$(docker::backup_description "${FDM_BACKUP}")")
}

function _fdm::backup_screen::refresh_action_list() {
    backup_screen_data__options+=("Restore service [d]ata"); backup_screen_data__option_restore="${backup_screen_data__options[-1]}"

    if [[ "${backup_screen_data__is_persistent}" -eq 0 ]]; then
        backup_screen_data__options+=("Make [a]nonymous"); backup_screen_data__option_make_anonymous="${backup_screen_data__options[-1]}"
    else
        backup_screen_data__options+=("Make [p]ersistent"); backup_screen_data__option_make_persistent="${backup_screen_data__options[-1]}"
    fi

    backup_screen_data__options+=("R[e]move this backup"); backup_screen_data__option_remove="${backup_screen_data__options[-1]}"
    backup_screen_data__options+=("Display [i]nfo"); backup_screen_data__option_info="${backup_screen_data__options[-1]}"
    backup_screen_data__options+=("[R]efresh screen"); backup_screen_data__option_refresh="${backup_screen_data__options[-1]}"
    backup_screen_data__options+=("[B]ack"); backup_screen_data__option_back="${backup_screen_data__options[-1]}"
    backup_screen_data__options+=("[Q]uit"); backup_screen_data__option_quit="${backup_screen_data__options[-1]}"
}

function fdm::backup_screen::header() {
    fdm::message::introduce_current_screen
    echo
    echo -n 'This backup is '

    # shellcheck disable=SC2015
    [[ "${backup_screen_data__is_persistent}" -eq 0 ]] && fdm::theme::persistent 'persistent' || fdm::theme::anonymous 'anonymous' 

    echo -n " and takes up $(fdm::theme::dynamic "${backup_screen_data__totalsize}") of disk space."
    echo
    
    if [[ "${backup_screen_data__is_persistent}" -eq 0 ]]; then
        echo
        echo "Description: \"$(fdm::theme::dynamic "${backup_screen_data__description}")\""
    fi
}

function fdm::backup_screen::menu() {
    fdm::message::how_to_execute_operations 1 ${#backup_screen_data__options[@]}
    echo

    local readonly _choice=$(prompt::choice "${backup_screen_data__options[@]}")
    case "${_choice}" in
       "${backup_screen_data__option_info}" )
            fdm::run::function 'fdm::backup_screen::validate' '_fdm::backup_screen::display_backup_info' "${FDM_BACKUP}"
            ;;
        "${backup_screen_data__option_restore}" )
            echo "Restoring service data from '$(basename "${FDM_BACKUP}")' to service '${FDM_SERVICE}'..."
            echo "Current service data will be overwritten!"
            fdm::run::confirmed_script 'fdm::backup_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/restore_service_data" "${FDM_SERVICE}" "${FDM_BACKUP}"
            ;;
        "${backup_screen_data__option_remove}" )
            echo "Deleting backup '$(basename "${FDM_BACKUP}")' of service '${FDM_SERVICE}'..."
            echo "This operation cannot be undone!"
            prompt::request_confirmation_yN || return
            fdm::run::script 'fdm::backup_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/delete_service_backup" "${FDM_SERVICE}" "$(basename "${FDM_BACKUP}")"
            if [[ ! -d "${FDM_BACKUP}" ]]; then
                # shellcheck disable=SC2034
                FDM_SCREEN='all_backups_screen'
                # shellcheck disable=SC2034
                FDM_BACKUP=''
            fi
            ;;
        "${backup_screen_data__option_make_persistent}" )
            echo "Changing a backup's type from 'anonymous' to 'persistent' will prevent $(fdm::theme::application "${FDM_APPLICATION_NAME}") to remove this backup automatically. It is mandatory for a persistent backup to have a description."
            echo
            if [[ $? -eq 0 ]]; then
                local readonly _description=$(prompt::input_not_empty 'Please enter a description')
                echo "Making '$(basename "${FDM_BACKUP}")' persistent..."
                echo "Description read from STDIN: \"$(fdm::theme::dynamic "$(text::sanitize_and_untabify "${_description}")")\""
                _description=${_description//\"/\\\"}
                fdm::run::confirmed_function 'fdm::backup_screen::validate' '_fdm::backup_screen::make_backup_persistent' "\"${_description}\""
            fi
            ;;
        "${backup_screen_data__option_make_anonymous}" )
            echo "Changing a backup's type from 'persistent' to 'anonymous' will allow $(fdm::theme::application "${FDM_APPLICATION_NAME}") to remove this backup automatically when it becomes too old."
            fdm::run::confirmed_function 'fdm::backup_screen::validate' '_fdm::backup_screen::anonymize_backup'
            ;;
        "${backup_screen_data__option_refresh}" )
            ;;
        "${backup_screen_data__option_back}" )
            # shellcheck disable=SC2034
            FDM_SCREEN='all_backups_screen'
            # shellcheck disable=SC2034
            FDM_BACKUP=''
            ;;
        "${backup_screen_data__option_quit}")
            exit 0
            ;;
    esac
}

function _fdm::backup_screen::display_backup_info() {
    util::mandatory_arg_count 1 "$@"
    local readonly _backup="$1"
    local readonly _desc=$(text::sanitize_and_untabify "$(docker::backup_description "${_backup}")")

    echo
    echo "Full path:"
    fdm::theme::dynamic "${_backup}"; echo

    echo
    echo "Size:"
    fdm::theme::dynamic "${backup_screen_data__totalsize}"; echo

    echo
    echo "Description:"
    if [[ -n ${_desc} ]]; then
        fdm::theme::dynamic "${_desc}"; echo
    else
        echo "Anonymous, non-persistent backup"
    fi
 }

function _fdm::backup_screen::anonymize_backup() {
    local readonly _markerfile=$(docker::persistent_backup_marker "${FDM_BACKUP}")

    if ! docker::is_persistent_backup "${FDM_BACKUP}"; then
        log::info "Backup is already anonymous; nothing to do"
        return
    fi

    rm -f "${_markerfile}"
    if [[ $? -ne 0 ]]; then 
        log::error "Backup was not anonymized; could not remove persistent marker file: ${_markerfile}" 
    else
        log::info "Backup is now anonymous"
    fi
}

function _fdm::backup_screen::make_backup_persistent() {
    util::mandatory_arg_count 1 "$@"
    local readonly _description="$1"

    if docker::is_persistent_backup "${FDM_BACKUP}"; then
        log::info "Backup is already persistent; nothing to do"
        return
    fi

    local readonly _markerfile=$(docker::persistent_backup_marker "${FDM_BACKUP}")

    util::create_file "${_markerfile}" 640
    if [[ $? -ne 0 ]]; then
        log::error "Backup was not made permanent; could not create persistent marker file: ${_markerfile}"
        return
    fi

    echo -n "${_description}" > "${_markerfile}"
    if [[ $? -ne 0 ]]; then
        rm -f "${_markerfile}"
        if [[ "$?" -ne 0 ]]; then
            log::error "Inconsistent state: permanent backup with empty description; remove this file manually: ${_markerfile}"
        else
            log::error 'Backup was not made permanent; could not save description'
        fi
    else
        log::info 'Backup is now persistent'
    fi
}

function fdm::backup_screen::validate() {
    fdm::validation::check_conditions_for_all_backups_screen

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        # shellcheck disable=SC2034
        FDM_BACKUP=''
        # shellcheck disable=SC2034
        FDM_SCREEN='all_backups_screen'
        return 1
    fi

    return 0
}

