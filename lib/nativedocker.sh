# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_nativedocker_sh_included}" ]] && _nativedocker_sh_included='yes' || return

source "${FRIENDLY_BASH}"

readonly DOCKER_CONTAINER_NAME_PATTERN='[a-zA-Z0-9][a-zA-Z0-9_.-]+'

function nativedocker::docker_is_running() {
    service snap.docker.dockerd status > /dev/null 2>&1
    [[ $? -ne 0 ]] && return 1 || return 0
}

function nativedocker::container_exists() {
    util::mandatory_arg_count 1 "$@"
    local readonly _potential_container="$1"

    docker ps -a | tail -n +2 | grep --quiet -E "\s${_potential_container}$"
    return $?
}

function nativedocker::container_is_running() {
    util::mandatory_arg_count 1 "$@"
    local readonly _container="$1"

    docker ps | tail -n +2 | grep --quiet -E "\s${_container}$"
    return $?
}

function nativedocker::container_image() {
    util::mandatory_arg_count 1 "$@"
    local readonly _container="$1"

    docker ps -a | tail -n +2 | grep -E "\s${_container}$" | tr -s ' ' | cut -d ' ' -f 2
}

function nativedocker::is_valid_container_name() {
    util::mandatory_min_arg_count 1 "$@"

    [[ -z "$1" ]] && return 1
    [[ "$#" -gt 1 ]] && return 1

    [[ "$1" =~ ^${DOCKER_CONTAINER_NAME_PATTERN}$ ]] && return 0 || return 1
}

function nativedocker::stop_all_running_containers() {
    util::recommended_arg_count 0 "$@"
    
    # shellcheck disable=SC2119
    if nativedocker::running_containers_exist; then
        # shellcheck disable=SC2046
        docker stop $(docker ps -q)
    fi
}


# shellcheck disable=SC2120
function nativedocker::running_containers_exist() {
    util::recommended_arg_count 0 "$@"

    [[ $(docker ps | wc -l) -gt 1 ]] && return 0 || return 1
}

# shellcheck disable=SC2120
function nativedocker::list_installed_images() {
    util::recommended_arg_count 0 "$@"

    docker images | tail -n +2 | tr -s ' ' | cut -d ' ' -f 1,2 | tr ' ' ':' | tr '\n' ' '
}

function nativedocker::image_is_installed() {
    util::mandatory_arg_count 1 "$@"
    local readonly _image="$1"

    # shellcheck disable=SC2119
    local readonly _all_images=($(nativedocker::list_installed_images))
    local _img
    for _img in ${_all_images[*]}; do
        [[ "${_img}" = "${_image}" ]] && return 0
    done

    return 1
}

function nativedocker::create_data_container() {
    util::mandatory_arg_count 2 "$@"
    local readonly _name="$1"
    local readonly _image="$2"
    if [[ -z "${_image}" || ! "${_image}" =~ ^[^\ ]+$ ]]; then
        log::error "Invalid image name: '${_image}'"
        return 1
    fi
    if ! nativedocker::image_is_installed "${_image}"; then
        log::error "Image '${_image}' is not installed"
        return 1
    fi

    docker create --name="${_name}" --entrypoint=true "${_image}" > /dev/null
}

function nativedocker::replace_data_container() {
    util::mandatory_arg_count 2 "$@"
    local readonly _current_data_container="$1"
    local readonly _new_data_container="$2"
        
    if ! nativedocker::container_exists "${_current_data_container}"; then
        log::error "Container does not exist: '${_current_data_container}'"
        return 1
    fi

    if ! nativedocker::is_valid_container_name "${_new_data_container}"; then
        log::error "Invalid container name: '${_new_data_container}'"
        return 1
    fi

    local readonly _image=$(nativedocker::container_image "${_current_data_container}")

    docker create --name="${_new_data_container}" --volumes-from="${_current_data_container}" --entrypoint=true "${_image}" > /dev/null

    if ! nativedocker::container_exists "${_new_data_container}"; then
        log::error "Container could not be created: '${_new_data_container}'"
        return 1
    fi
    log::debug "Container created: '${_new_data_container}'"
    
    docker rm "${_current_data_container}" > /dev/null
    if nativedocker::container_exists "${_current_data_container}"; then
        log::error "Container could not be removed: '${_current_data_container}'"
        return 1
    fi
    log::debug "Container removed: '${_current_data_container}'"

    return 0    
}

function nativedocker::remove_data_container() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    if [[ -z "${_data_container}" ]]; then
        log::error "No data container received"
        return 1
    fi
    if ! nativedocker::container_exists "${_data_container}"; then
        log::error "Data container '${_data_container}' does not exist"
        return 1
    fi
    
    docker rm --volumes=true "${_data_container}" > /dev/null
}

function nativedocker::remove_service_container() {
    util::mandatory_arg_count 1 "$@"
    local readonly _service_container="$1"
    if [[ -z "${_service_container}" ]]; then
        log::error "No service container received"
        return 1
    fi
    if ! nativedocker::container_exists "${_service_container}"; then
        log::error "Service container '${_service_container}' does not exist"
        return 1
    fi

    if nativedocker::container_is_running "${_service_container}"; then
        log::error "Service container '${_service_container}' is running and cannot be removed"
        return 1
    fi
    
    docker rm "${_service_container}" > /dev/null
}

function nativedocker::wait_for_container_start() {
    util::mandatory_arg_count 2 "$@"
    local readonly _start_time_millis="$(date +%s%3N)"
    local readonly _container="$1"
    local readonly _timeout_in_seconds="$2"
    local readonly _timeout_in_millis="$((_timeout_in_seconds * 1000 ))"

    log::debug "Waiting ${_timeout_in_seconds} seconds for container '${_container}' to start..."
    while true
    do
        local _current_time_millis="$(date +%s%3N)"
        local _time_passed_in_millis="$(( _current_time_millis - _start_time_millis ))"
    
        if nativedocker::container_is_running "${_container}"; then
            log::debug "Container '${_container}' started after ${_time_passed_in_millis} milliseconds"
            return 0
        else
            if [ ${_time_passed_in_millis} -ge ${_timeout_in_millis} ]; then
                log::debug "Starting container '${_container}' timed out"
                return 1
            else
                sleep 0.1
            fi
        fi
    done
}

function nativedocker::wait_for_container_stop() {
    util::mandatory_arg_count 2 "$@"
    local readonly _start_time_millis="$(date +%s%3N)"
    local readonly _container="$1"
    local readonly _timeout_in_seconds="$2"
    local readonly _timeout_in_millis="$((_timeout_in_seconds * 1000 ))"

    log::debug "Waiting ${_timeout_in_seconds} seconds for container '${_container}' to stop..."
    while true
    do
        local _current_time_millis="$(date +%s%3N)"
        local _time_passed_in_millis="$(( _current_time_millis - _start_time_millis ))"

        if nativedocker::container_is_running "${_container}"; then
            if [ ${_time_passed_in_millis} -ge ${_timeout_in_millis} ]; then
                log::debug "Stopping container '${_container}' timed out"
                return 1
            else
                sleep 0.1
            fi
        else
            log::debug "Container '${_container}' stopped after ${_time_passed_in_millis} milliseconds"
            return 0
        fi
    done
}

function nativedocker::connect_to_container_terminal() {
    util::mandatory_arg_count 1 "$@"
    local readonly _container="$1"
    if [[ -z "${_container}" ]]; then
        log::error "No container provided!"
        return 1
    fi
    if ! nativedocker::container_is_running "${_container}"; then
        log::error "Container '${_container}' is not running!"
        return 1
    fi
    
    docker exec -it "${_container}" bash
}

