# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_theme_sh_included}" ]] && _fdm_theme_sh_included='yes' || return

source "${FRIENDLY_BASH}"

# IMPORTANT!
# All these functions will reset previous format attributes to their default values.

function fdm::theme::application() {
    format::default
    format::set "${COLOUR_LIGHT_BLUE}"
    echo -n "$*"
    format::default
}

function fdm::theme::title_bar() {
    format::default
    echo -n "$(fdm::theme::application "[${BACKGROUND_WHITE};${STYLE_INVERTED}m$*")"
    format::default
}

function fdm::theme::dynamic() {
    format::default
    format::set "${COLOUR_LIGHT_BLUE}"
    echo -n "$*"
    format::default
}

function fdm::theme::action() {
    format::default
    format::set "${COLOUR_WHITE}"
    echo -n "$*" | tr -d '[' | tr -d ']'
    format::default
}

function fdm::theme::started() {
    format::default
    format::set "${COLOUR_GREEN}"
    echo -n "$*"
    format::default
}

function fdm::theme::stopped() {
    format::default
    format::set "${COLOUR_MAGENTA}"
    echo -n "$*"
    format::default
}

function fdm::theme::needs_repair() {
    format::default
    format::set "${COLOUR_LIGHT_CYAN}" "${BACKGROUND_MAGENTA}"
    echo -n "$*"
    format::default
}

function fdm::theme::invalid() {
    format::default
    format::set "${COLOUR_RED}"
    echo -n "$*"
    format::default
}

function fdm::theme::persistent() {
    format::default
    format::set "${COLOUR_YELLOW}"
    echo -n "$*"
    format::default
}

function fdm::theme::anonymous() {
    format::default
    format::set "${COLOUR_WHITE}"
    echo -n "$*"
    format::default
}

