# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_message_sh_included}" ]] && _fdm_message_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"

function fdm::message::logs_can_be_found_here() {
    echo "The full log of this action can be found here: ${DOCKER_MAIN_LOG_FILE}"
    echo "The console output displayed above was saved here: ${DOCKER_CONSOLE_DUMP_FILE}"
}

function fdm::message::how_to_execute_operations() {
    util::mandatory_arg_count 2 "$@"

    local _lower_bound="$1"
    local _upper_bound="$2"

    echo "To perform an action, type either the action number ($(fdm::theme::dynamic "${_lower_bound}..${_upper_bound}")) or its shortcut at the prompt. Shortcuts are case insensitive."
    echo
    echo "Press ENTER to execute your choice."
}

function fdm::message::introduce_current_screen() {
    case ${FDM_SCREEN} in
        'main_screen' | 'invalid_config_files_screen' | '' )
            # not applicable
            ;;
        'service_screen' | 'config_files_screen' | 'all_backups_screen' )
            echo "You have entered the $(fdm::message::screen_name "${FDM_SCREEN}") for service '$(fdm::theme::dynamic "${FDM_SERVICE}")'"
            ;;
        'backup_screen')
            echo "You have entered the $(fdm::message::screen_name "${FDM_SCREEN}") for '$(fdm::theme::dynamic "$(basename "${FDM_BACKUP}")")' of service '$(fdm::theme::dynamic "${FDM_SERVICE}")'"
            ;;
    esac       
}

function fdm::message::screen_name() {
    util::mandatory_arg_count 1 "$@"

    local readonly _screen="$1"

    case ${_screen} in
        'main_screen' )
            echo -n 'main screen'
            ;;
        'service_screen' )
            echo -n 'service management screen'
            ;;
        'config_files_screen' )
            echo -n 'configuration files screen'
            ;;
        'invalid_config_files_screen' )
            echo -n 'invalid configuration files screen'
            ;;
        'all_backups_screen' )
            echo -n 'data management screen'
            ;;
        'backup_screen' )
            echo -n 'backup management screen'
            ;;
        '' )
            echo -n 'error screen'
            ;;
       esac 
}

function fdm::message::current_screen_title() {
    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        echo -n 'Error!'
    else
        case "${FDM_SCREEN}" in
            'main_screen' )
                ;;
            'service_screen' )
                echo -n "${FDM_SERVICE} >> Manage service"
                ;;
            'config_files_screen' )
                echo -n "${FDM_SERVICE} >> Edit service configuration files"
                ;;
            'invalid_config_files_screen' )
                echo -n "${FDM_SERVICE} >> Error in service configuration files"
                ;;
            'all_backups_screen' )
                echo -n "${FDM_SERVICE} >> Manage service data"
                ;;
            'backup_screen' )
                echo -n "${FDM_SERVICE} >> $(basename "${FDM_BACKUP}")"
                ;;
            * )
                echo -n 'Unknown error!'
                ;;
        esac
    fi
}

