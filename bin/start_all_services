#!/bin/bash 

# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

# Usage: start_all_services
#
# Starts all Docker services.

source "${FRIENDLY_BASH}"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

docker::lock || exit 1
trap "docker::unlock" EXIT

readonly this_script="$(basename "$0")"

log::new_section
log::debug "Starting '${this_script}' with arguments [$*]..."

docker::mandatory_repair_data_directory

util::recommended_arg_count 0 "$@"

docker::refresh_service_list; readonly all_data_containers=("${FRIENDLY_DOCKER_SERVICES[@]}")

if [[ ${#all_data_containers[@]} -eq 0 ]]; then
    log::info "No services initialized; nothing to start"
    exit 0
fi

for data_container in "${all_data_containers[@]}"; do
    if ! docker::is_valid_service "${data_container}"; then
        log::warning "Service '${data_container}' is invalid!"
        continue
    fi

    if docker::should_skip_start "${data_container}"; then
        log::info "Service '${data_container}' was skipped"
        continue
    fi

    ${DOCKER_INTERNAL_START_SERVICE_SCRIPT} "${data_container}"
done

log::debug "Finished '${this_script}'"

