# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_main_screen_sh_included}" ]] && _fdm_main_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/nativedocker.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/run.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/validation.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/properties.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/display.sh"

function fdm::main_screen::refresh_data() {
    _fdm::main_screen::init_data
    _fdm::main_screen::refresh_service_list_and_data
    _fdm::main_screen::refresh_action_list
}

function _fdm::main_screen::init_data() {
    main_screen_data__service_count=0

    main_screen_data__started_count=0
    main_screen_data__stopped_count=0
    main_screen_data__invalid_count=0

    main_screen_data__needs_export_count=0
    main_screen_data__needs_repair_count=0

    main_screen_data__options=()
    main_screen_data__services=()

    local _trash="${DOCKER_REMOVED_DATA_ROOT}"
    if [[ -d "${_trash}" && -n "$(ls -A "${_trash}")" ]]; then
        main_screen_data__trash_size=$(util::totalsize_human_readable "${_trash}")
   else
        main_screen_data__trash_size=''
    fi
}

function _fdm::main_screen::refresh_service_list_and_data() {
    docker::refresh_service_list; main_screen_data__services=("${FRIENDLY_DOCKER_SERVICES[@]}")

    local _service
    for _service in "${main_screen_data__services[@]}"; do
        local _service_name="$(text::sanitize_and_untabify "${_service}")"
        local _service_option

        if docker::is_valid_service "${_service}"; then
            if docker::service_is_running "${_service}"; then
                _service_option=$(fdm::theme::started "${_service_name}")
                (( main_screen_data__started_count++ ))
            else
                if docker::service_needs_container_repair "${_service}"; then
                    _service_option=$(fdm::theme::needs_repair "${_service_name}")
                    (( main_screen_data__needs_repair_count++ ))
                else
                    _service_option=$(fdm::theme::stopped "${_service_name}")
                fi
                (( main_screen_data__stopped_count++ ))
            fi

            if docker::has_export_marker "${_service}" && ! docker::should_skip_export "${_service}"; then
                _service_option="${_service_option}*"
                (( main_screen_data__needs_export_count++ ))
            fi

         else
            _service_option=$(fdm::theme::invalid "${_service_name}")
            (( main_screen_data__invalid_count++ ))
        fi

        main_screen_data__options+=("${_service_option}")
        (( main_screen_data__service_count++ ))
    done
}

function _fdm::main_screen::refresh_action_list() {
    if [[ "${main_screen_data__needs_repair_count}" -gt 0 ]]; then
        main_screen_data__options+=("Re[p]air all services")
        main_screen_data__option_repair_all="${main_screen_data__options[-1]}"
    fi

    main_screen_data__options+=("[N]ew service")
    main_screen_data__option_new_service="${main_screen_data__options[-1]}"

    if [[ "${main_screen_data__service_count}" -gt 0 ]]; then

        if [[ "${main_screen_data__stopped_count}" -gt 0 ]]; then
            main_screen_data__options+=("[S]tart all services")
            main_screen_data__option_start_all="${main_screen_data__options[-1]}"
        fi

        if [[ "${main_screen_data__started_count}" -gt 0 ]]; then
            main_screen_data__options+=("S[t]op all services")
            main_screen_data__option_stop_all="${main_screen_data__options[-1]}"
        fi

        if [[ "${main_screen_data__service_count}" -gt "${main_screen_data__invalid_count}" ]]; then
            main_screen_data__options+=("B[a]ck up all services")
            main_screen_data__option_export_all="${main_screen_data__options[-1]}"
        fi
        
        if [[ "${main_screen_data__needs_export_count}" -gt 0 ]]; then
            main_screen_data__options+=("R[e]try pending backups")
            main_screen_data__option_retry_all="${main_screen_data__options[-1]}"
        fi

        main_screen_data__options+=("Space [u]sage report")
        main_screen_data__option_disk_usage="${main_screen_data__options[-1]}" 
    fi

    main_screen_data__options+=("General [i]nfo")
    main_screen_data__option_display_info="${main_screen_data__options[-1]}"

    main_screen_data__options+=("[R]efresh screen")
    main_screen_data__option_refresh="${main_screen_data__options[-1]}"

    main_screen_data__options+=("[Q]uit")
    main_screen_data__option_quit="${main_screen_data__options[-1]}"
}

function fdm::main_screen::header() {
    echo "Welcome to the $(fdm::theme::application "${FDM_APPLICATION_NAME}"), a text-based interface for managing Docker services!"
    echo
    echo -n "Found $(fdm::theme::dynamic "${main_screen_data__service_count}") service(s)"
    
    [[ "${main_screen_data__service_count}" -eq 0 ]] && return

    echo -n ": $(fdm::theme::dynamic "${main_screen_data__started_count}") $(fdm::theme::started 'started')"
    echo -n ", $(fdm::theme::dynamic "${main_screen_data__stopped_count}") $(fdm::theme::stopped 'stopped')"

    if [[ "${main_screen_data__invalid_count}" -gt 0 ]]; then
        echo -n ", $(fdm::theme::dynamic "${main_screen_data__invalid_count}") $(fdm::theme::invalid 'invalid')"
    fi
    echo

    if [[ "${main_screen_data__needs_export_count}" -gt 0 ]]; then
        echo
        echo "Found $(fdm::theme::dynamic "${main_screen_data__needs_export_count}") service(s) with backup pending (*)"
    fi

    if [[ "${main_screen_data__needs_repair_count}" -gt 0 ]]; then
        fdm::display::horizontal_line
        fdm::theme::needs_repair 'At least one service needs container repair!'
        echo
        echo "Services needing repair cannot be started until the problem is fixed."
        echo "Run $(fdm::theme::action "${main_screen_data__option_repair_all}") to fix the problem for all the affected services."
        echo
        echo "This issue might be caused by a failure to stop services before system shutdown."
    fi

    if [[ -n "${main_screen_data__trash_size}" ]]; then
        fdm::display::horizontal_line
        echo -n "There are currently $(fdm::theme::dynamic "${main_screen_data__trash_size}") of removed data. "
        echo "Run $(fdm::theme::action "${main_screen_data__option_display_info}") to see where the removed data is stored."
    fi
}

function fdm::main_screen::menu() {
    if [[ "${main_screen_data__service_count}" -gt 0 ]]; then
        echo "To enter the $(fdm::message::screen_name 'service_screen'), type the service number ($(fdm::theme::dynamic "1..${main_screen_data__service_count}")) at the prompt."
        echo
    fi

    fdm::message::how_to_execute_operations "$((main_screen_data__service_count+1))" "${#main_screen_data__options[@]}"
    echo

    local readonly _choice=$(prompt::choice "${main_screen_data__options[@]}")
    case "${_choice}" in
        "${main_screen_data__option_refresh}" )
            ;;
        "${main_screen_data__option_start_all}" )
            echo 'Starting all services...'
            fdm::run::confirmed_script 'fdm::main_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/start_all_services"
            ;;
        "${main_screen_data__option_stop_all}" )
            echo 'Stoping all services...'
            fdm::run::confirmed_script 'fdm::main_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/stop_all_services"
            ;;
        "${main_screen_data__option_export_all}" )
            echo 'Exporting all service data...'
            fdm::run::confirmed_script 'fdm::main_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/export_all_service_data"
            ;;
        "${main_screen_data__option_retry_all}" )
            echo 'Retrying failed exports for all services...'
            fdm::run::confirmed_script 'fdm::main_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/retry_all_export_service_data"
            ;;
        "${main_screen_data__option_repair_all}" )
            echo 'Repairing all affected services... Their service containers will be deleted!'
            fdm::run::confirmed_function 'fdm::main_screen::validate' '_fdm::main_screen::repair_all'
            ;;
        "${main_screen_data__option_new_service}" )
            echo 'Creating a new service...'
            prompt::request_confirmation_Yn 'Do you want to continue?'
            if [[ "$?" -eq 0 ]]; then
                local readonly _images=($(nativedocker::list_installed_images))

                echo
                echo "Choose an image for the new service:"
                echo
                local readonly _image=$(prompt::choice "${_images[@]}")

                local _data_container=$(prompt::input_not_empty 'Data container')
                _data_container=${_data_container//\"/\\\"}

                local _service_container=$(prompt::input_not_empty 'Service container')
                _service_container=${_service_container//\"/\\\"}

                local _service_description=$(prompt::input 'Service description')
                _service_description=${_service_description//\\/\\\\}
                _service_description=${_service_description//\'/\'\\\'\'}
                _service_description=${_service_description//\"/\\\"}

                echo
                echo "Creating service '$(text::sanitize_and_untabify "${_data_container}")' [image='${_image}', data_container='$(text::sanitize_and_untabify "${_data_container}")', service_container='$(text::sanitize_and_untabify "${_service_container}")']..."

                fdm::run::confirmed_script 'fdm::main_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/init_service" "${_image}" "\"${_data_container}\"" "\"${_service_container}\"" "\"${_service_description}\""
                if docker::service_is_initialized "${_data_container}"; then
                    # shellcheck disable=SC2034
                    FDM_SCREEN='config_files_screen'
                    # shellcheck disable=SC2034
                    FDM_SERVICE="${_data_container}"
                fi
            fi
            ;;
        "${main_screen_data__option_disk_usage}" )
            fdm::run::function 'fdm::main_screen::validate' '_fdm::main_screen::display_disk_usage'
            ;;
        "${main_screen_data__option_display_info}" )
            fdm::run::function 'fdm::main_screen::validate' '_fdm::main_screen::display_info'
            ;;
        "${main_screen_data__option_quit}" )
            exit 0
            ;;
        * )
            for (( i=0; i < main_screen_data__service_count; i++ )); do
                if [[ "${main_screen_data__options[$i]}" = "${_choice}" ]]; then
                    # shellcheck disable=SC2034
                    FDM_SCREEN='service_screen'
                    # shellcheck disable=SC2034
                    FDM_SERVICE="${main_screen_data__services[$i]}"
                    break;
                fi
            done
            ;;
    esac
}

function _fdm::main_screen::repair_all() {
    docker::refresh_service_list; local readonly _services=("${FRIENDLY_DOCKER_SERVICES[@]}")

    local _service
    for _service in "${_services[@]}"; do
        if docker::service_needs_container_repair "${_service}"; then
            docker::repair_service_container "${_service}"
        fi
    done
}

function _fdm::main_screen::display_disk_usage() {
    docker::refresh_service_list; local readonly _services=("${FRIENDLY_DOCKER_SERVICES[@]}")
    
    echo
    display::horizontal_line
    echo 'Space taken by service data'
    display::horizontal_line

    local _total_size=$(util::totalsize_human_readable "${DOCKER_BACKUPS_ROOT}")

    local _results=''
    local _width=0

    local _service
    for _service in "${_services[@]}"; do
        local _container_backups_root=$(docker::container_backups_root "${_service}")
        local _size=0
        local _size_human_readable=0
        if [[ -d "${_container_backups_root}" ]]; then
            _size=$(util::totalsize "${_container_backups_root}")
            _size_human_readable=$(util::totalsize_human_readable "${_container_backups_root}")
        fi

        local _printable_service=$(text::sanitize_and_untabify "${_service}")
        (( ${#_printable_service} > _width )) && _width="${#_printable_service}"
       _results="${_results}${_size}|${_printable_service}|${_size_human_readable}\n"
    done

    # prevent results that contain spaces from being split into different array items
    local readonly _old_IFS="${IFS}"
    IFS=$'\n'

    _results=($(echo -e "${_results}" | sort -gr))

    IFS="${_old_IFS}"

    local i=0
    for _result in "${_results[@]}"; do
        [[ -z "${_result}" ]] && continue

        (( i++ ))
        local _service_name=$(echo "${_result}" | cut -d '|' -f 2)
        local _size=$(echo "${_result}" | cut -d '|' -f 3)
        printf "%3s. %-${_width}s %s\n" "$i" "${_service_name}" "$(fdm::theme::dynamic "${_size}")"
    done

    display::horizontal_line
    printf "%3s  %-${_width}s %s\n" ' ' 'TOTAL' "$(fdm::theme::dynamic "${_total_size}")"
    display::horizontal_line
}

function _fdm::main_screen::display_info() {
    echo
    echo "Log file:"
    fdm::theme::dynamic "${DOCKER_MAIN_LOG_FILE}"; echo
    echo
    echo "Console dump:"
    fdm::theme::dynamic "${DOCKER_CONSOLE_DUMP_FILE}"; echo

    if [[ -n "${main_screen_data__trash_size}" ]]; then
        echo
        echo "Removed data:"
        fdm::theme::dynamic "${DOCKER_REMOVED_DATA_ROOT}"; echo
    fi
}

function fdm::main_screen::validate() {
    fdm::validation::check_conditions_for_main_screen

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        # shellcheck disable=SC2034
        FDM_SCREEN=''
        return 1
    fi

    return 0
}

