# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_display_sh_included}" ]] && _fdm_display_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/properties.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"

function fdm::display::loading() {
    clear
    fdm::display::title_bar
    echo
    format::set "${COLOUR_WHITE}"
    echo "Loading..."
    fdm::display::horizontal_line
    format::default
}

function fdm::display::horizontal_line() {
    echo
    fdm::theme::application "$(display::horizontal_line)"
    echo
    echo
}

function fdm::display::title_bar() {
    local readonly _specific_title="$(fdm::message::current_screen_title)"
    local readonly _version=$(cat "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/.version")
    local readonly _generic_title="${FDM_APPLICATION_NAME} ${_version}"
    local readonly _screen_width=$(display::get_screen_width)

    local _minimal_title="${_specific_title} | ${_generic_title}"
    if [[ ${#_minimal_title} -gt ${_screen_width} ]]; then
        # shellcheck disable=SC2088
        _minimal_title="$(display::trim_to_screen "${_minimal_title}" '~')"
    else
        _minimal_title=''
        local readonly _padding=$(( _screen_width - ${#_specific_title} - ${#_generic_title} ))
    fi

    local _title
    if [[ -n "${_minimal_title}" ]]; then
        _title=$(printf "%s" "${_minimal_title}")
    else
        _title=$(printf "%s%${_padding}s%s" "${_specific_title}" " " "${_generic_title}")
    fi

    fdm::theme::title_bar "${_title}"; echo
}

