# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_docker_sh_included}" ]] && _docker_sh_included='yes' || return

source "${FRIENDLY_BASH}"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/nativedocker.sh"

readonly DOCKER_LOCKFILE=~/.friendlydocker_lock
readonly DOCKER_CONTAINERS_FILE=${FRIENDLY_DOCKER_DATA_ROOT}/.containers

readonly DOCKER_BACKUPS_ROOT=${FRIENDLY_DOCKER_DATA_ROOT}/Backups
readonly DOCKER_CONFIG_ROOT=${FRIENDLY_DOCKER_DATA_ROOT}/Config
readonly DOCKER_LOGS_ROOT=${FRIENDLY_DOCKER_DATA_ROOT}/Logs
readonly DOCKER_BACKUP_LOGS_ROOT=${DOCKER_LOGS_ROOT}/backup
readonly DOCKER_MAIN_LOG_FILE=${DOCKER_LOGS_ROOT}/docker.log
readonly DOCKER_CONSOLE_DUMP_FILE=${DOCKER_LOGS_ROOT}/docker.dump
readonly DOCKER_ISSUES_ROOT=${FRIENDLY_DOCKER_DATA_ROOT}/Issues
readonly DOCKER_REMOVED_DATA_ROOT=${FRIENDLY_DOCKER_DATA_ROOT}/.removed

readonly DOCKER_PERSISTENT_BACKUP_MARKER_FILENAME=".keepme"
readonly DOCKER_INCOMPLETE_BACKUP_MARKER_FILENAME=".incomplete"
readonly DOCKER_EXPORT_MARKER_FILENAME=".needs_export"

readonly DOCKER_INTERNAL_START_SERVICE_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_start_service
readonly DOCKER_INTERNAL_STOP_SERVICE_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_stop_service
readonly DOCKER_INTERNAL_EXPORT_SERVICE_DATA_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_export_service_data
readonly DOCKER_INTERNAL_RESTORE_SERVICE_DATA_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_restore_service_data
readonly DOCKER_INTERNAL_CLEAN_EXPORTED_DATA_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_clean_exported_data
readonly DOCKER_INTERNAL_INIT_SERVICE_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_init_service
readonly DOCKER_INTERNAL_REMOVE_SERVICE_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_remove_service
readonly DOCKER_INTERNAL_RENAME_SERVICE_SCRIPT=${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/internal/do_rename_service

readonly DOCKER_DEFAULT_ANONYMOUS_BACKUP_HISTORY_SIZE=5
readonly DOCKER_DEFAULT_CONTAINER_START_WAIT_TIME=10
readonly DOCKER_DEFAULT_CONTAINER_STOP_WAIT_TIME=60

function docker::lock() {
    # Note:
    # All logging inside this function should be suppressed, except log::lock_error messages,
    # to improve the readability of log files.

    if [[ -f "${DOCKER_LOCKFILE}" ]]; then
        local readonly _locking_script=$(cat "${DOCKER_LOCKFILE}")
        log::lock_error "Script '$(identify::this_script)' could not acquire lock; it is already owned by '${_locking_script}'"
        return 1
    fi

    eval "util::create_file ${DOCKER_LOCKFILE} 640 > /dev/null 2>&1 ${LOG_STREAM}>&1 ${ISSUES_LOG_STREAM}>&1"
    if [[ $? -ne 0 ]]; then
        local readonly _lock_parent_dir=$(dirname "${DOCKER_LOCKFILE}")
        if [[ -d "${_lock_parent_dir}" ]]; then
            log::lock_error "Script '$(identify::this_script)' could not acquire lock; lock file could not be created: ${DOCKER_LOCKFILE}"
            return 1
        fi

        eval "util::create_dir ${_lock_parent_dir} 750 > /dev/null 2>&1 ${LOG_STREAM}>&1 ${ISSUES_LOG_STREAM}>&1"
        if [[ $? -ne 0 ]]; then
            log::lock_error "Script '$(identify::this_script)' could not acquire lock; lock file parent directory could not be created: ${_lock_parent_dir}"
            return 1
        fi
        
        eval "util::create_file ${DOCKER_LOCKFILE} 640 > /dev/null 2>&1 ${LOG_STREAM}>&1 ${ISSUES_LOG_STREAM}>&1"
        if [[ $? -ne 0 ]]; then
            log::lock_error "Script '$(identify::this_script)' could not acquire lock; lock file could not be created: ${DOCKER_LOCKFILE}"
            return 1
        fi
    fi
    
    echo "$(identify::this_script)(PID=$$)" > "${DOCKER_LOCKFILE}"
}

function docker::lock_with_timeout() {
    # Note:
    # All logging inside this function should be suppressed, except log::lock_error messages,
    # to improve the readability of log files.

    util::mandatory_arg_count 1 "$@"
    local readonly _start_time_millis="$(date +%s%3N)"
    local readonly _timeout_in_seconds="$1"
    local readonly _timeout_in_millis="$((_timeout_in_seconds * 1000 ))"
 
    while true; do
        if [[ ! -a "${DOCKER_LOCKFILE}" ]]; then
            docker::lock && return 0
        else
            local _current_time_millis="$(date +%s%3N)"
            local _time_passed_in_millis="$(( _current_time_millis - _start_time_millis ))"
            if [ ${_time_passed_in_millis} -ge ${_timeout_in_millis} ]; then
                return 1
            fi
        fi
        sleep 0.1
    done
}

function docker::unlock() {
    if [[ -f "${DOCKER_LOCKFILE}" ]]; then
        rm -f "${DOCKER_LOCKFILE}"
        if [[ $? -ne 0 ]]; then
            log::lock_error "Script '$(identify::this_script)' failed to release lock"
            return 1
        fi 
    else 
        log::lock_error "Script '$(identify::this_script)' tried to release lock, but lock was not found"
    fi

    return 0
}

function docker::export_marker_file() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"

    echo "$(docker::container_backups_root "${_data_container}")/${DOCKER_EXPORT_MARKER_FILENAME}"
}

function docker::has_export_marker() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"

    [[ -f "$(docker::export_marker_file "${_data_container}")" ]] && return 0 || return 1
}

function docker::create_export_marker() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"

    if docker::has_export_marker "${_data_container}"; then
        log::info "Export marker for '${_data_container}' already exists so no need to create it"
    else
        local readonly _file=$(docker::export_marker_file "${_data_container}")
        util::create_file "${_file}" 440
        if [[ $? -ne 0 ]]; then
            log::error "Failed to create export marker for '${_data_container}'"
            return 1
        else
            log::info "Export marker successfully created for '${_data_container}'"
        fi
    fi

    return 0
}

function docker::delete_export_marker() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"

    if ! docker::has_export_marker "${_data_container}"; then
        log::info "No export marker for '${_data_container}' so no need to delete it"
    else
        local readonly _file=$(docker::export_marker_file "${_data_container}")
        rm -f "${_file}"
        if [[ $? -ne 0 ]]; then
            log::error "Failed to delete export marker for '${_data_container}'"
            return 1
        else
            log::info "Export marker successfully deleted for '${_data_container}'"
        fi
    fi

    return 0
}


# shellcheck disable=SC2120
function docker::refresh_service_list() {
    util::recommended_arg_count 0 "$@"

    FRIENDLY_DOCKER_SERVICES=()
    
    local _line
    while IFS=$'\r' read -r _line
    do
        [[ "${_line}" =~ ^[[:space:]]*$ ]] && continue
        FRIENDLY_DOCKER_SERVICES+=("${_line}")
    done < "${DOCKER_CONTAINERS_FILE}"
    [[ ! "${_line}" =~ ^[[:space:]]*$ ]] && FRIENDLY_DOCKER_SERVICES+=("${_line}")
}

function docker::initialize_service() {
    util::mandatory_arg_count 1 "$@"
    local readonly _service="$1"

    if [ -s "${DOCKER_CONTAINERS_FILE}" ] && [ "$(tail -c1 "${DOCKER_CONTAINERS_FILE}"; echo x)" != $'\nx' ]; then
        echo >> "${DOCKER_CONTAINERS_FILE}"
    fi
    echo "${_service}" >> "${DOCKER_CONTAINERS_FILE}"
}

function docker::uninitialize_service() {
    util::mandatory_arg_count 1 "$@"
    local readonly _service="$1"

    sed -i "/^${_service}\?\$/d" "${DOCKER_CONTAINERS_FILE}"
}

function docker::rename_initialized_service() {
    util::mandatory_arg_count 2 "$@"
    local readonly _current_service="$1"
    local readonly _new_service="$2"

    sed -i "s/^${_current_service}\?\$/${_new_service}/" "${DOCKER_CONTAINERS_FILE}"
}

function docker::service_is_initialized() {
    util::mandatory_arg_count 1 "$@"
    local readonly _service_name="$1"

    grep --quiet "^${_service_name}\?$" "${DOCKER_CONTAINERS_FILE}"
    return $?
}

function docker::service_is_running() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    
    local readonly _service_container=$(docker::service_container_name "${_data_container}")
    [[ -z "${_service_container}" ]] && return 1
    
    nativedocker::container_is_running "${_service_container}" && return 0 || return 1
}

function docker::check_service() {
    local readonly _service_name="$1"
    local readonly _printable_service_name="$(text::sanitize_and_untabify "${_service_name}")"
    local readonly _service_container=$(docker::service_container_name "${_service_name}") 
    local readonly _printable_service_container="$(text::sanitize_and_untabify "${_service_container}")"
    local readonly _start_script=$(docker::container_specific_start_script "${_service_name}")
    local readonly _stop_script=$(docker::container_specific_stop_script "${_service_name}")
    local readonly _export_script=$(docker::container_specific_export_script "${_service_name}")
    local readonly _restore_script=$(docker::container_specific_restore_script "${_service_name}")

    if [[ -z "${_service_name}" ]]; then
        echo 'No service name received'

    elif ! docker::service_is_initialized "${_service_name}"; then
        echo "'${_printable_service_name}' is not an initialized service"

    elif ! nativedocker::container_exists "${_service_name}"; then
        echo "Data container for service '${_printable_service_name}' does not exist"

    elif nativedocker::container_is_running "${_service_name}"; then
        echo "Data container for service '${_service_name}' is running"

    elif [[ -z "${_service_container}" ]]; then
        echo "Could not read service container name for service '${_service_name}'"

    elif ! nativedocker::is_valid_container_name "${_service_container}"; then
        echo "Invalid name for service container: '${_printable_service_container}'; must match pattern ${DOCKER_CONTAINER_NAME_PATTERN}"

    elif [[ "${_service_name}" = "${_service_container}" ]]; then
        echo "The name of the service container for '${_service_name}' is also the name of the service"

    elif [[ ! -f ${_start_script} ]]; then
        echo "Could not find container specific start script for service '${_service_name}'"
    
    elif [[ ! -f ${_stop_script} ]]; then
        echo "Could not find container specific stop script for service '${_service_name}'"

    elif [[ ! -f ${_export_script} ]]; then
        echo "Could not find container specific export script for service '${_service_name}'"

    elif [[ ! -f ${_restore_script} ]]; then
        echo "Could not find container specific restore script for service '${_service_name}'"

    else
        local _init_count=0
        # shellcheck disable=SC2119
        docker::refresh_service_list; local readonly _initialized_data_containers=("${FRIENDLY_DOCKER_SERVICES[@]}")
        local _initialized_data_container
        for _initialized_data_container in "${_initialized_data_containers[@]}"; do
            local _printable_initialized_data_container="$(text::sanitize_and_untabify "${_initialized_data_container}")"
            if [[ "${_service_name}" = "${_initialized_data_container}" ]]; then
                if [[ ${_init_count} -eq 0 ]]; then
                    (( _init_count++ ))
                    continue
                else
                    echo "Service '${_service_name}' found more than once in the list of initialized services"
                    break
                fi
            fi

            _initialized_service_container="$(docker::service_container_name "${_initialized_data_container}")"
            if [[ "${_service_name}" = "${_initialized_service_container}" ]]; then
                echo "Service name conflict: '${_service_name}' is also the service container for service '${_printable_initialized_data_container}'"
                break
            fi

            if [[ "${_service_container}" = "${_initialized_data_container}" ]]; then
                echo "Service container name conflict: '${_service_container}' is also the name of another service" 
                break
            fi

            if [[ "${_service_container}" = "${_initialized_service_container}" ]]; then
                echo "Service container name conflict: '${_service_container}' is also the service container for service '${_printable_initialized_data_container}'"
                break
            fi
        done
    fi
}

function docker::is_valid_service() {
    local readonly _error_message=$(docker::check_service "$1")
    if [[ -n "${_error_message}" ]]; then
        return 1
    else
        return 0
    fi
}

function docker::validate_service() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"

    local readonly _error_message=$(docker::check_service "${_data_container}")
    [[ -n "${_error_message}" ]] && util::fatal_error "${_error_message}"
}

function docker::container_backups_root() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "${DOCKER_BACKUPS_ROOT}/${_data_container}"
}

function docker::container_backup_dir() {
    util::mandatory_arg_count 2 "$@"
    local readonly _data_container="$1"
    local readonly _backup_id="$2"

    echo "$(docker::container_backups_root "${_data_container}")/backup.${_backup_id}"
}

function docker::backup_archive_name() {
    util::mandatory_arg_count 1 "$@"
    local readonly _path="$1"
    util::is_absolute_path "${_path}" || util::fatal_error "Path to compute backup archive name is not absolute: '${_path}'"

    echo "$(util::compute_hash "${_path}").tar"
}

function docker::incomplete_export_marker_file() {
    util::mandatory_arg_count 2 "$@"
    local readonly _data_container="$1"
    local readonly _backup_id="$2"    
   
    echo "$(docker::container_backup_dir "${_data_container}" "${_backup_id}")/${DOCKER_INCOMPLETE_BACKUP_MARKER_FILENAME}"
}

function docker::generate_backup_id() {
    util::generate_timestamp_id
}

function docker::persistent_backup_marker() {
    util::mandatory_arg_count 1 "$@"
    local readonly _backup_dir="$1"
    util::is_absolute_path "${_backup_dir}" || util::fatal_error "Not an absolute path: ${_backup_dir}"

    echo "${_backup_dir}/${DOCKER_PERSISTENT_BACKUP_MARKER_FILENAME}"
}

function docker::temporary_persistent_backup_marker() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"

    echo "$(docker::container_backups_root "${_data_container}")/${DOCKER_PERSISTENT_BACKUP_MARKER_FILENAME}"
}

function docker::is_persistent_backup() {
    util::mandatory_arg_count 1 "$@"
    local readonly _backup_dir="$1"
    util::is_absolute_path "${_backup_dir}" || util::fatal_error "Not an absolute path: ${_backup_dir}"

    [[ -f "$(docker::persistent_backup_marker "${_backup_dir}")" ]]
}

function docker::backup_description() {
    util::mandatory_arg_count 1 "$@"
    local readonly _backup_dir="$1"
    util::is_absolute_path "${_backup_dir}" || util::fatal_error "Not an absolute path: ${_backup_dir}"

    cat "$(docker::persistent_backup_marker "${_backup_dir}")" 2> /dev/null
}

function docker::service_backups_log_file() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "${DOCKER_BACKUP_LOGS_ROOT}/${_data_container}_backups.log"
}

function docker::service_properties_file() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _container_config_dir="$(docker::container_config_dir "${_data_container}")"
    
    echo "${_container_config_dir}/.properties"
}

function docker::service_description() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"
    
    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ -n "${DOCKER_SERVICE_DESCRIPTION}" ]]; then
            echo "${DOCKER_SERVICE_DESCRIPTION}"
            return 0
        fi
    fi

    return 1
}


function docker::service_container_name() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"
    
    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ -n "${DOCKER_SERVICE_CONTAINER_NAME}" ]]; then
            echo "${DOCKER_SERVICE_CONTAINER_NAME}"
            return 0
        fi
    fi

    return 1
}

function docker::service_anonymous_backup_history_size() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"

    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ -n "${DOCKER_ANONYMOUS_BACKUP_HISTORY_SIZE}" && "${DOCKER_ANONYMOUS_BACKUP_HISTORY_SIZE}" =~ ^[1-9][0-9]*$ ]]; then
            echo "${DOCKER_ANONYMOUS_BACKUP_HISTORY_SIZE}"
            return 0
        fi
    fi
    
    echo "${DOCKER_DEFAULT_ANONYMOUS_BACKUP_HISTORY_SIZE}"
    return 0
}

function docker::service_start_wait_time() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"

    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ -n "${DOCKER_CONTAINER_START_WAIT_TIME}" && "${DOCKER_CONTAINER_START_WAIT_TIME}" =~ ^[1-9][0-9]*$ ]]; then
            echo "${DOCKER_CONTAINER_START_WAIT_TIME}"
            return 0
        fi
    fi
    
    echo "${DOCKER_DEFAULT_CONTAINER_START_WAIT_TIME}"
    return 0
}

function docker::service_stop_wait_time() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"

    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ -n "${DOCKER_CONTAINER_STOP_WAIT_TIME}"  && "${DOCKER_CONTAINER_STOP_WAIT_TIME}" =~ ^[1-9][0-9]*$ ]]; then
            echo "${DOCKER_CONTAINER_STOP_WAIT_TIME}"
            return 0
        fi
    fi
    
    echo "${DOCKER_DEFAULT_CONTAINER_STOP_WAIT_TIME}"
    return 0
}

function docker::should_skip_start() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"

    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ "${DOCKER_SKIP_FROM_START_ALL}" = 'true' ]]; then
            return 0
        fi
    fi
    
    return 1
}

function docker::should_skip_export() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _properties_file="$(docker::service_properties_file "${_data_container}")"

    if [[ -f ${_properties_file} ]]; then
        source "${_properties_file}"
        if [[ "${DOCKER_SKIP_FROM_EXPORT_ALL}" = 'true' ]]; then
            return 0
        fi
    fi
    
    return 1
}

function docker::container_config_dir() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "${DOCKER_CONFIG_ROOT}/${_data_container}"
}

function docker::container_specific_export_script() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "$(docker::container_config_dir "${_data_container}")/export_${_data_container}"
}

function docker::container_specific_restore_script() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "$(docker::container_config_dir "${_data_container}")/restore_${_data_container}"
}

function docker::container_specific_start_script() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "$(docker::container_config_dir "${_data_container}")/start_${_data_container}"
}

function docker::container_specific_stop_script() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
   
    echo "$(docker::container_config_dir "${_data_container}")/stop_${_data_container}"
}

function docker::run_script() {
    util::mandatory_min_arg_count 1 "$@"
    local readonly _script="$1"
    local readonly _args="${*:2}"

    local readonly _issues_file="${DOCKER_ISSUES_ROOT}/$(util::generate_timestamp_id)_$(basename "${_script}")"
    local readonly _issues_file_pending="${_issues_file}.pending"

    eval "setsid ${_script} ${_args} ${LOG_STREAM}>>${DOCKER_MAIN_LOG_FILE} ${ISSUES_LOG_STREAM}>>${_issues_file_pending} 2>&1" | tee -a "${DOCKER_CONSOLE_DUMP_FILE}"
    local readonly _status="$?"

    if [[ -s ${_issues_file_pending} ]]; then
        mv "${_issues_file_pending}" "${_issues_file}"
    else
        rm -f "${_issues_file_pending}"
    fi

    return "${_status}"
}

function docker::run_export_script() {
    util::mandatory_arg_count 1 "$@"
    local readonly _data_container="$1"
    local readonly _export_script="${DOCKER_INTERNAL_EXPORT_SERVICE_DATA_SCRIPT}"
    local readonly _log_file="$(docker::service_backups_log_file "${_data_container}")"

    log::info "Running export script; messages will be logged in: ${_log_file}"
    eval "${_export_script} ${_data_container} ${LOG_STREAM}>>${_log_file}"
}

function docker::run_restore_script() {
    util::mandatory_arg_count 2 "$@"
    local readonly _data_container="$1"
    local readonly _backup_dir="$2"
    local readonly _restore_script="${DOCKER_INTERNAL_RESTORE_SERVICE_DATA_SCRIPT}"
    local readonly _log_file="$(docker::service_backups_log_file "${_data_container}")"

    log::info "Running restore script; messages will be logged in: ${_log_file}"
    eval "${_restore_script} ${_data_container} ${_backup_dir} ${LOG_STREAM}>>${_log_file}"
}

# shellcheck disable=SC2120
function docker::is_data_directory_valid() {
    util::recommended_arg_count 0 "$@"
    [[ ! -d "${FRIENDLY_DOCKER_DATA_ROOT}" ]] && return 1
    [[ ! -d "${DOCKER_BACKUPS_ROOT}" ]] && return 1
    [[ ! -d "${DOCKER_CONFIG_ROOT}" ]] && return 1
    [[ ! -d "${DOCKER_LOGS_ROOT}" ]] && return 1
    [[ ! -d "${DOCKER_BACKUP_LOGS_ROOT}" ]] && return 1
    [[ ! -d "${DOCKER_ISSUES_ROOT}" ]] && return 1
    
    [[ -d "${DOCKER_MAIN_LOG_FILE}" ]] && return 1
    [[ -d "${DOCKER_CONSOLE_DUMP_FILE}" ]] && return 1
    
    [[ ! -f "${DOCKER_CONTAINERS_FILE}" ]] && return 1
    
    return 0
}

# shellcheck disable=SC2120
function docker::repair_data_directory() {
    util::recommended_arg_count 0 "$@"

    # shellcheck disable=SC2119
    docker::is_data_directory_valid && return 0

    log::info "Data directory needs repairs; it might have been changed; repairing..."
    
    if [[ ! -d "${FRIENDLY_DOCKER_DATA_ROOT}" ]]; then
        util::create_dir "${FRIENDLY_DOCKER_DATA_ROOT}" 750
        if [[ $? -ne 0 ]]; then
            log::error "Could not create data directory: '${FRIENDLY_DOCKER_DATA_ROOT}'"
            return 1
        fi
        log::debug "Created data directory: '${FRIENDLY_DOCKER_DATA_ROOT}'"
    fi

    if [[ ! -d "${DOCKER_BACKUPS_ROOT}" ]]; then
        util::create_dir "${DOCKER_BACKUPS_ROOT}" 750
        if [[ $? -ne 0 ]]; then
            log::error "Could not create backups root directory: '${DOCKER_BACKUPS_ROOT}'"
            return 1
        fi
        log::debug "Created backups root directory: '${DOCKER_BACKUPS_ROOT}'"
    fi


    if [[ ! -d "${DOCKER_CONFIG_ROOT}" ]]; then
        util::create_dir "${DOCKER_CONFIG_ROOT}" 750
        if [[ $? -ne 0 ]]; then
            log::error "Could not create config root directory: '${DOCKER_CONFIG_ROOT}'"
            return 1
        fi
        log::debug "Created config root directory: '${DOCKER_CONFIG_ROOT}'"
    fi

    if [[ ! -d "${DOCKER_LOGS_ROOT}" ]]; then
        util::create_dir "${DOCKER_LOGS_ROOT}" 750
        if [[ $? -ne 0 ]]; then
            log::error "Could not create logs root directory: '${DOCKER_LOGS_ROOT}'"
            return 1
        fi
        log::debug "Created logs root directory: '${DOCKER_LOGS_ROOT}'"
    fi

    if [[ ! -d "${DOCKER_BACKUP_LOGS_ROOT}" ]]; then
        util::create_dir "${DOCKER_BACKUP_LOGS_ROOT}" 750
        if [[ $? -ne 0 ]]; then
            log::error "Could not create backup logs root directory: '${DOCKER_BACKUP_LOGS_ROOT}'"
            return 1
        fi
        log::debug "Created backup logs root directory: '${DOCKER_BACKUP_LOGS_ROOT}'"
    fi

    if [[ ! -d "${DOCKER_ISSUES_ROOT}" ]]; then
        util::create_dir "${DOCKER_ISSUES_ROOT}" 750
        if [[ $? -ne 0 ]]; then
            log::error "Could not create issues root directory: '${DOCKER_ISSUES_ROOT}'"
            return 1
        fi
        log::debug "Created issues root directory: '${DOCKER_ISSUES_ROOT}'"
    fi
    
    if [[ -d "${DOCKER_MAIN_LOG_FILE}" ]]; then
        log::error "Could not create main log file as there is already a directory with that name: '${DOCKER_MAIN_LOG_FILE}'"
        return 1
    fi

    if [[ -d "${DOCKER_CONSOLE_DUMP_FILE}" ]]; then
        log::error "Could not create console dump file as there is already a directory with that name: '${DOCKER_CONSOLE_DUMP_FILE}'"
    fi
    
    if [[ ! -f "${DOCKER_CONTAINERS_FILE}" ]]; then
        util::create_file "${DOCKER_CONTAINERS_FILE}" 640
        if [[ $? -ne 0 ]]; then
            log::error "Could not create initialized containers file: '${DOCKER_CONTAINERS_FILE}'"
            return 1
        fi
        log::debug "Created initialized containers file: '${DOCKER_CONTAINERS_FILE}'"
    fi

    log::info "Data directory repaired successfully"
    echo
    return 0
}

function docker::mandatory_repair_data_directory() {
    util::recommended_arg_count 0 "$@"

    # shellcheck disable=SC2119
    docker::repair_data_directory
    [[ $? -eq 0 ]] || util::fatal_error 'Could not repair data directory'
}

function docker::service_needs_container_repair() {
    util::mandatory_arg_count 1 "$@"
    local readonly _service="$1"
    docker::is_valid_service "${_service}" || return 1

    local readonly _service_container=$(docker::service_container_name "${_service}")
    if nativedocker::container_exists "${_service_container}" && ! nativedocker::container_is_running "${_service_container}"; then
        return 0
    fi

    return 1
}

function docker::repair_service_container() {
    util::mandatory_arg_count 1 "$@"
    local readonly _service="$1"
    ! docker::service_needs_container_repair "${_service}" && return 0
    
    local readonly _service_container=$(docker::service_container_name "${_service}")
    nativedocker::remove_service_container "${_service_container}"
    local readonly _status="$?"

    if [[ "${_status}" -eq 0 ]]; then
        log::info "Service '${_service}' was repaired; its service container '${_service_container}' was deleted "
    else
        log::error "An error occured when repairing service '${_service}'"
    fi

    return "${_status}"
}

