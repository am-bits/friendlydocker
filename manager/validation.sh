# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_validation_sh_included}" ]] && _fdm_validation_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/nativedocker.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/properties.sh"

function fdm::validation::check_conditions_for_main_screen() {
    _fdm::validation::reset_error_reason

    if ! util::program_is_installed 'docker'; then
        FDM_ERROR_REASON="Docker is not installed."
        return
    fi

    if ! nativedocker::docker_is_running; then
        FDM_ERROR_REASON="Docker is not running."
        return
    fi

    docker::repair_data_directory > /dev/null 2>&1
    if ! docker::is_data_directory_valid; then
        FDM_ERROR_REASON="The '${FDM_APPLICATION_NAME}' data directory is not valid and could not be repaired."
        return
    fi
}

function fdm::validation::check_conditions_for_service_screen() {
    fdm::validation::check_conditions_for_main_screen
    [[ -n "${FDM_ERROR_REASON}" ]] && return

    FDM_ERROR_REASON=$(docker::check_service "${FDM_SERVICE}")
    [[ -n "${FDM_ERROR_REASON}" ]] && return
}

function fdm::validation::check_conditions_for_all_backups_screen() {
    fdm::validation::check_conditions_for_service_screen
    [[ -n "${FDM_ERROR_REASON}" ]] && return

    # no specific conditions
}

function fdm::validation::check_conditions_for_backup_screen() {
    fdm::validation::check_conditions_for_all_backups_screen
    [[ -n "${FDM_ERROR_REASON}" ]] && return

    if [[ ! -d "${FDM_BACKUP}" ]]; then
        FDM_ERROR_REASON="Backup directory '${FDM_BACKUP}' does not exist."
        return
    fi
}

function fdm::validation::check_conditions_for_config_files_screen() {
    fdm::validation::check_conditions_for_service_screen
    [[ -n "${FDM_ERROR_REASON}" ]] && return

    if [[ -z "${FRIENDLY_DOCKER_EDITOR}" ]]; then
        FDM_ERROR_REASON="\$FRIENDLY_DOCKER_EDITOR variable is not defined."
        return
    fi

    if ! util::program_is_installed "${FRIENDLY_DOCKER_EDITOR}"; then
        FDM_ERROR_REASON="Editor '${FRIENDLY_DOCKER_EDITOR}' is not installed."
        return
    fi
    
    if docker::service_is_running "${FDM_SERVICE}"; then
        FDM_ERROR_REASON="Service '${FDM_SERVICE}' is started so editing configuration files is not permitted."
        return 
    fi

    if docker::service_needs_container_repair "${FDM_SERVICE}"; then
        FDM_ERROR_REASON="Service '${FDM_SERVICE}' needs container repair so editing configuration files is not permitted."
        return 
    fi
}

function fdm::validation::check_conditions_for_invalid_config_files_screen() {
    fdm::validation::check_conditions_for_main_screen
    [[ -n "${FDM_ERROR_REASON}" ]] && return

    if [[ -z "${FRIENDLY_DOCKER_EDITOR}" ]]; then
        FDM_ERROR_REASON="\$FRIENDLY_DOCKER_EDITOR variable is not defined."
        return
    fi

    if ! util::program_is_installed "${FRIENDLY_DOCKER_EDITOR}"; then
        FDM_ERROR_REASON="Editor '${FRIENDLY_DOCKER_EDITOR}' is not installed."
        return
    fi
}

 function _fdm::validation::reset_error_reason() {
    FDM_ERROR_REASON=''
}
