# FriendlyDocker #

FriendlyDocker is a suite of shell scripts that aim to simplify the management of services running in [Docker](https://www.docker.com/) containers.

It also provides an interactive text-based user interface, the *Friendly Docker Manager*.

![fdm__main_screen.png](https://bitbucket.org/repo/zGzar8/images/63897262-fdm__main_screen.png)

## How to install ##

1. Go to the *Dependencies* section below and check that everything listed there is installed.

1. Download the [latest version](https://bitbucket.org/am-bits/friendlydocker/downloads/friendlydocker_latest.tar) of FriendlyDocker. Older versions can be found [here](https://bitbucket.org/am-bits/friendlydocker/downloads).

1. Extract the contents of the tar archive at a chosen location on your machine, e.g.

        tar -xvf friendlydocker_latest.tar -C /my/path/to/friendlydocker

1. Add the following definitions to your `/etc/environment` file:

        FRIENDLY_DOCKER_SCRIPTS_ROOT=/my/path/to/friendlydocker
		FRIENDLY_DOCKER_DATA_ROOT=/my/path/to/docker/service/data
		FRIENDLY_DOCKER_EDITOR=/my/favourite/editor

## How to use ##

### Friendly Docker Manager ###

Before using the Friendly Docker Manager, you should have the [Docker images](https://docs.docker.com/engine/reference/commandline/images/) that you need already installed in Docker.

To start the Friendly Docker Manager, run:

	$FRIENDLY_DOCKER_SCRIPTS_ROOT/docker_service_manager

#### Creating services ####

Create a new service by following the on-screen instructions. (Hint: Read everything on the screen; it should be obvious what to do.)

At some point, you will be asked to provide a name for the "data container". Note that this name will also be used as the name of the service.

After the service is created, you will be taken to the service configuration screen. Do not skip service configuration!

#### Configuring services ####

A newly created service must be configured before you can use it.

At the least, you will have to edit the **start script**, the **export script** and the **restore script** that Friendly Docker Manager generated. Before making any changes, read all the comments and hints in those files carefully!

#### Managing services ####

Once a service is created and configured, it can be completely managed using the Friendly Docker Manager:

* start and stop the service
* back up and restore data
* manage existing backups
* change the configuration of the service

#### Removing services ####

You can remove a service that is no longer needed by going to its service screen. Note that the service data will be archived, not deleted, so it can be recovered if you change your mind.

### The Friendly Docker Manager "back end" ###

The scripts that power the Friendly Docker Manager can be found in `$FRIENDLY_DOCKER_SCRIPTS_ROOT/bin`.

These scripts can be used independently of the Friendly Docker Manager. For example, you can create a [*systemd*](https://wiki.archlinux.org/index.php/systemd) service for starting all Docker services at system start-up, and for stopping all Docker services before system shut-down.

### Other scripts ###

To back up all Docker services and shut down, run:

	$FRIENDLY_DOCKER_SCRIPTS_ROOT/docker_backup_and_shutdown

To shutdown only if no Docker operation is in progress, run:

	$FRIENDLY_DOCKER_SCRIPTS_ROOT/shutdown_if_possible

To reboot only if no Docker operation is in progress, run:

	$FRIENDLY_DOCKER_SCRIPTS_ROOT/reboot_if_possible

## Dependencies ##

* [Docker](https://www.docker.com) v1.9.1
* [FriendlyBash](https://bitbucket.org/am-bits/friendlybash) v1.2

## Development ##

### Setup ###

1. Clone the repository.

1. Add the following definitions to your `/etc/environment` file:

        FRIENDLY_DOCKER_SCRIPTS_ROOT=/my/path/to/friendlydocker/repository
		FRIENDLY_DOCKER_DATA_ROOT=/my/path/to/docker/service/data
		FRIENDLY_DOCKER_EDITOR=/my/favourite/editor

1. Install [ShellCheck](https://github.com/koalaman/shellcheck).

### Development guidelines ###

Same as the [FriendlyBash](https://bitbucket.org/am-bits/friendlybash) development guidelines.
