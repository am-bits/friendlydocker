# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_service_screen_sh_included}" ]] && _fdm_service_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/nativedocker.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/run.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/validation.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/display.sh"

function fdm::service_screen::refresh_data() {
    _fdm::service_screen::init_data
    _fdm::service_screen::refresh_service_data
    _fdm::service_screen::refresh_action_list
}

function _fdm::service_screen::init_data() {
    service_screen_data__options=()
}

function _fdm::service_screen::refresh_service_data() {
    if docker::service_is_running "${FDM_SERVICE}"; then
        service_screen_data__is_running=0
    else
        service_screen_data__is_running=1
    fi

    if docker::has_export_marker "${FDM_SERVICE}" && ! docker::should_skip_export "${FDM_SERVICE}"; then
        service_screen_data__needs_export=0
    else
        service_screen_data__needs_export=1
    fi

    if docker::service_needs_container_repair "${FDM_SERVICE}"; then
        service_screen_data__needs_repair=0
    else
        service_screen_data__needs_repair=1
    fi

    service_screen_data__total_data_size=$(util::totalsize_human_readable "$(docker::container_backups_root "${FDM_SERVICE}")")
}

function _fdm::service_screen::refresh_action_list() {
    if [[ "${service_screen_data__is_running}" -eq 0 ]]; then
        service_screen_data__options+=("S[t]op service")
        service_screen_data__option_stop="${service_screen_data__options[-1]}"

        service_screen_data__options+=("Restart ser[v]ice")
        service_screen_data__option_restart="${service_screen_data__options[-1]}"
        
        service_screen_data__options+=("[C]onnect to service")
        service_screen_data__option_connect="${service_screen_data__options[-1]}"
     else
        if [[ "${service_screen_data__needs_repair}" -eq 0 ]]; then
            service_screen_data__options+=("Re[p]air service")
            service_screen_data__option_repair="${service_screen_data__options[-1]}"
        else
            service_screen_data__options+=("[S]tart service")
            service_screen_data__option_start="${service_screen_data__options[-1]}"
            
            service_screen_data__options+=("Edit confi[g] files")
            service_screen_data__option_config="${service_screen_data__options[-1]}"
        fi
    fi

    service_screen_data__options+=("Manage [d]ata ($(fdm::theme::dynamic "${service_screen_data__total_data_size}"))")
    service_screen_data__option_backups="${service_screen_data__options[-1]}"

    service_screen_data__options+=("Re[n]ame service")
    service_screen_data__option_rename="${service_screen_data__options[-1]}"

    service_screen_data__options+=("R[e]move service")
    service_screen_data__option_remove="${service_screen_data__options[-1]}"

    service_screen_data__options+=("Display [i]nfo")
    service_screen_data__option_info="${service_screen_data__options[-1]}"
 
    service_screen_data__options+=("[R]efresh screen")
    service_screen_data__option_refresh="${service_screen_data__options[-1]}"
    
    service_screen_data__options+=("[B]ack")
    service_screen_data__option_back="${service_screen_data__options[-1]}"
    
    service_screen_data__options+=("[Q]uit")
    service_screen_data__option_quit="${service_screen_data__options[-1]}"
}

function fdm::service_screen::header() {
    fdm::message::introduce_current_screen
    echo
    if [[ "${service_screen_data__is_running}" -eq 0 ]]; then
        echo "This service is $(fdm::theme::started 'started')."
    else
        echo "This service is $(fdm::theme::stopped 'stopped')."

        if [[ "${service_screen_data__needs_repair}" -eq 0 ]]; then
            fdm::display::horizontal_line
            fdm::theme::needs_repair 'This service needs container repair!'; echo
            echo
            echo "Services needing repair cannot be started until the problem is fixed."
            echo "Run $(fdm::theme::action "${service_screen_data__option_repair}") to fix the problem."
            echo
            echo "This issue might be caused by a failure to stop the service before system shutdown."
        fi
    fi

    if [[ "${service_screen_data__needs_export}" -eq 0 ]]; then
        fdm::display::horizontal_line
        echo "There is a pending backup for this service."
    fi
}

function fdm::service_screen::menu() {
    fdm::message::how_to_execute_operations '1' "${#service_screen_data__options[@]}"
    echo

    local _choice=$(prompt::choice "${service_screen_data__options[@]}")

    case "${_choice}" in
        "${service_screen_data__option_info}" )
            fdm::run::function 'fdm::service_screen::validate' '_fdm::service_screen::display_service_info' "${FDM_SERVICE}"
            ;;
        "${service_screen_data__option_start}" )
            echo "Starting service '${FDM_SERVICE}'..."
            fdm::run::confirmed_script 'fdm::service_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/start_service" "${FDM_SERVICE}"
            ;;
        "${service_screen_data__option_stop}" )
            echo "Stopping service '${FDM_SERVICE}'..."
            fdm::run::confirmed_script 'fdm::service_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/stop_service" "${FDM_SERVICE}"
            ;;
        "${service_screen_data__option_restart}" )
            echo "Restarting service '${FDM_SERVICE}'..."
            fdm::run::confirmed_script 'fdm::service_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/restart_service" "${FDM_SERVICE}"
            ;;
        "${service_screen_data__option_repair}" )
            local readonly _service_container_name="$(text::sanitize_and_untabify "$(docker::service_container_name "${FDM_SERVICE}")")"
            echo
            echo "Repairing service '${FDM_SERVICE}'... This will remove its Docker service container, '${_service_container_name}', and cannot be undone!"
            fdm::run::confirmed_function 'fdm::service_screen::validate' '_fdm::service_screen::repair_service'
            ;;
        "${service_screen_data__option_backups}" )
            # shellcheck disable=SC2034
            FDM_SCREEN='all_backups_screen'
            ;;
        "${service_screen_data__option_connect}" )
            if docker::service_is_running "${FDM_SERVICE}"; then
                local readonly _service_container=$(docker::service_container_name "${FDM_SERVICE}")
                echo "Connecting to Docker container '$(text::sanitize_and_untabify "${_service_container}")'..."

                PROMPT__KEEP_CURSOR_HIDDEN='false' # shellcheck disable=SC2034
                fdm::run::confirmed_function 'fdm::service_screen::validate' 'nativedocker::connect_to_container_terminal' "${_service_container}"
                PROMPT__KEEP_CURSOR_HIDDEN='true' # shellcheck disable=SC2034
            else
                echo
                echo "The service must be $(fdm::theme::started 'started') first, in order to connect to it."
                echo
                prompt::press_to_continue
            fi
            ;;
        "${service_screen_data__option_config}" )
            if ! docker::service_is_running "${FDM_SERVICE}"; then
                # shellcheck disable=SC2034
                FDM_SCREEN='config_files_screen'
            else
                echo
                echo "The service must be $(fdm::theme::stopped 'stopped') first, in order to edit its config files."
                echo
                prompt::press_to_continue
            fi
            ;;
        "${service_screen_data__option_rename}" )
            echo "Renaming service '${FDM_SERVICE}'..."
            prompt::request_confirmation_Yn || return

            echo
            local _new_service_name=$(prompt::input_not_empty 'New service name')
            _new_service_name=${_new_service_name//\"/\\\"}

            echo
            echo "Renaming service '${FDM_SERVICE}' to '$(text::sanitize_and_untabify "${_new_service_name}")'..."

            fdm::run::confirmed_script 'fdm::service_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/rename_service" "${FDM_SERVICE}" "\"${_new_service_name}\""
            if ! docker::service_is_initialized "${FDM_SERVICE}"; then
                if docker::service_is_initialized "${_new_service_name}"; then
                    # shellcheck disable=SC2034
                    FDM_SCREEN='config_files_screen'
                    # shellcheck disable=SC2034
                    FDM_SERVICE="${_new_service_name}"
                else
                    # shellcheck disable=SC2034
                    FDM_SCREEN='main_screen'
                    # shellcheck disable=SC2034
                    FDM_SERVICE=''
                fi
            fi
            ;;
        "${service_screen_data__option_remove}" )
            echo "Removing service '${FDM_SERVICE}'..."
            prompt::request_confirmation_yN || return
            fdm::run::script 'fdm::service_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/remove_service" "${FDM_SERVICE}"
            if ! docker::service_is_initialized "${FDM_SERVICE}"; then
                # shellcheck disable=SC2034
                FDM_SCREEN='main_screen'
                # shellcheck disable=SC2034
                FDM_SERVICE=''
            fi
            ;;
        "${service_screen_data__option_back}" )
            # shellcheck disable=SC2034
            FDM_SCREEN='main_screen'
            # shellcheck disable=SC2034
            FDM_SERVICE=''
            ;;
        "${service_screen_data__option_refresh}" )
            ;;
        "${service_screen_data__option_quit}" )
            exit 0
            ;;
        * )
            ;;
    esac
}

function fdm::service_screen::validate() {
    fdm::validation::check_conditions_for_service_screen

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        # shellcheck disable=SC2034
        FDM_SERVICE=''
        # shellcheck disable=SC2034
        FDM_SCREEN='main_screen'
        return 1
    fi

    return 0
}

function _fdm::service_screen::repair_service() {
    if docker::service_needs_container_repair "${FDM_SERVICE}"; then
        docker::repair_service_container "${FDM_SERVICE}"
    fi
}

function _fdm::service_screen::display_service_info() {
    local readonly _service="${FDM_SERVICE}"
    local readonly _service_container_name="$(text::sanitize_and_untabify "$(docker::service_container_name "${_service}")")"
    local readonly _service_description="$(text::sanitize_and_untabify "$(docker::service_description "${_service}")")"

    local _started_info
    docker::service_is_running "${_service}" && _started_info=$(fdm::theme::started 'yes') || _started_info=$(fdm::theme::stopped 'no')

    local _excluded_info
    docker::should_skip_start "${_service}" && _excluded_info='start all'
    if docker::should_skip_export "${_service}"; then
        [[ ! -z "${_excluded_info}" ]] && _excluded_info="${_excluded_info}, "
        _excluded_info="${_excluded_info}export all"
    fi

    local _info
    _info="${_info}Data container   :  $(fdm::theme::dynamic "${_service}")\n"
    _info="${_info}Service container:  $(fdm::theme::dynamic "${_service_container_name}")\n"
    _info="${_info}Description      :  '$(fdm::theme::dynamic "${_service_description}")'\n"
    _info="${_info}Started          :  ${_started_info}"
    
    if [[ -n "${_excluded_info}" ]]; then
        _info="${_info}\n"
        _info="${_info}Excluded from    :  $(fdm::theme::dynamic "${_excluded_info}")"
    fi

    echo
    echo -e "${_info}"
}

