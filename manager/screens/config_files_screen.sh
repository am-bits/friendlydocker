# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_config_files_screen_sh_included}" ]] && _fdm_config_files_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/run.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/validation.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/display.sh"

function fdm::config_files_screen::refresh_data() {
    config_files_screen_data__location=$(docker::container_config_dir "${FDM_SERVICE}")
    config_files_screen_data__properties_file=$(docker::service_properties_file "${FDM_SERVICE}")    
    config_files_screen_data__start_script=$(docker::container_specific_start_script "${FDM_SERVICE}")
    config_files_screen_data__stop_script=$(docker::container_specific_stop_script "${FDM_SERVICE}")
    config_files_screen_data__export_script=$(docker::container_specific_export_script "${FDM_SERVICE}")
    config_files_screen_data__restore_script=$(docker::container_specific_restore_script "${FDM_SERVICE}")

    config_files_screen_data__options=()

    config_files_screen_data__options+=("Open service [p]roperties file ($(fdm::theme::dynamic "$(basename "${config_files_screen_data__properties_file}")"))")
    config_files_screen_data__option_properties="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("Open [s]tart service script ($(fdm::theme::dynamic "$(basename "${config_files_screen_data__start_script}")"))")
    config_files_screen_data__option_start_script="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("Open s[t]op service script ($(fdm::theme::dynamic "$(basename "${config_files_screen_data__stop_script}")"))")
    config_files_screen_data__option_stop_script="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("Open e[x]port data script ($(fdm::theme::dynamic "$(basename "${config_files_screen_data__export_script}")"))")
    config_files_screen_data__option_export_script="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("Open restore [d]ata script ($(fdm::theme::dynamic "$(basename "${config_files_screen_data__restore_script}")"))")
    config_files_screen_data__option_restore_script="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("[R]efresh screen"); 
    config_files_screen_data__option_refresh="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("[B]ack"); 
    config_files_screen_data__option_back="${config_files_screen_data__options[-1]}"

    config_files_screen_data__options+=("[Q]uit"); 
    config_files_screen_data__option_quit="${config_files_screen_data__options[-1]}"
}


function fdm::config_files_screen::header() {
    fdm::message::introduce_current_screen
    echo
    echo "The configuration files for this service are stored at: '$(fdm::theme::dynamic "${config_files_screen_data__location}")'"
}

function fdm::config_files_screen::menu() {
    fdm::message::how_to_execute_operations '1' "${#config_files_screen_data__options[@]}"
    echo

    local _choice=$(prompt::choice "${config_files_screen_data__options[@]}")
    case "${_choice}" in
        "${config_files_screen_data__option_properties}" )
            _file="${config_files_screen_data__properties_file}"
            ;;
        "${config_files_screen_data__option_start_script}" )
            _file="${config_files_screen_data__start_script}"
            ;;
        "${config_files_screen_data__option_stop_script}" )
            _file="${config_files_screen_data__stop_script}"
            ;;
        "${config_files_screen_data__option_export_script}" )
            _file="${config_files_screen_data__export_script}"
            ;;
        "${config_files_screen_data__option_restore_script}" )
            _file="${config_files_screen_data__restore_script}"
            ;;
        "${config_files_screen_data__option_refresh}" )
            return
            ;;
         "${config_files_screen_data__option_back}" )
            # shellcheck disable=SC2034
            FDM_SCREEN='service_screen'
            return
            ;;
        "${config_files_screen_data__option_quit}" )
            exit 0
            ;;
        * )
            return
            ;;
    esac

    fdm::config_files_screen::validate || return
    fdm::run::file_editor "${_file}"
    fdm::display::loading

    if [[ "${_choice}" = "${config_files_screen_data__option_properties}" ]]; then
        local _error=$(docker::check_service "${FDM_SERVICE}")
        if [[ -n "${_error}" ]]; then
            # shellcheck disable=SC2034
            FDM_SCREEN='invalid_config_files_screen'
        fi
    fi
}

function fdm::config_files_screen::validate() {
    fdm::validation::check_conditions_for_config_files_screen

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        # shellcheck disable=SC2034
        FDM_SCREEN='service_screen'
        return 1
    fi

    return 0
}

