# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_all_backups_screen_sh_included}" ]] && _fdm_all_backups_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/lib/docker.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/message.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/run.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/validation.sh"

function fdm::all_backups_screen::refresh_data() {
    _fdm::all_backups_screen::init_data
    _fdm::all_backups_screen::refresh_backup_list_and_data
    _fdm::all_backups_screen::refresh_action_list
}

function _fdm::all_backups_screen::init_data() {    
    all_backups_screen_data__backups_count=0
    all_backups_screen_data__persistent_count=0
    all_backups_screen_data__anonymous_count=0

    all_backups_screen_data__options=()
}

function _fdm::all_backups_screen::refresh_backup_list_and_data() {
    local readonly _backups_root=$(docker::container_backups_root "${FDM_SERVICE}")
    
    all_backups_screen_data__total_size=$(util::totalsize_human_readable "${_backups_root}")

    # filter out backup folders that contain the incomplete backup marker
    local readonly _backups=($(find "${_backups_root}" -mindepth 1 -maxdepth 1 -type d '!' -exec test -e "{}/${DOCKER_INCOMPLETE_BACKUP_MARKER_FILENAME}" ';' -print | sort -r))

    # shellcheck disable=SC2088
    local readonly _terminator='~' 

    local _backup
    for _backup in "${_backups[@]}"; do
        local readonly _backup_name=$(basename "${_backup}")

        local _option
        if docker::is_persistent_backup "${_backup}"; then
            _option=$(_fdm::all_backups_screen::make_persistent_backup_option "${_backup}")
            all_backups_screen_data__persistent_count=$(( all_backups_screen_data__persistent_count+1))
        else
            _option=$(_fdm::all_backups_screen::make_anonymous_backup_option "${_backup}")
            all_backups_screen_data__anonymous_count=$(( all_backups_screen_data__anonymous_count+1))
        fi

        all_backups_screen_data__options+=("${_option}")
        all_backups_screen_data__backups_count=$(( all_backups_screen_data__backups_count+1))
    done
}

function _fdm::all_backups_screen::make_persistent_backup_option() {
    util::mandatory_arg_count 1 "$@"
    local readonly _backup="$1"
    local readonly _backup_name=$(basename "${_backup}")

    local readonly _desc="$(docker::backup_description "${_backup}")"

    local _option
    _option="${_backup_name} ($(util::totalsize_human_readable "${_backup}"))"
    _option="${_option}: ${_desc}"
    _option=$(display::trim_column "${_option}" ${_terminator})
    if [[ "${#_option}" -le "${#_backup_name}" ]]; then
        # do not trim to less than the backup name; we will need it in full to enter the backup screen
        _option=$(fdm::theme::persistent "${_backup_name}")
    else
        # apply theme to the backup name only
        _option=$(fdm::theme::persistent "${_option:0:${#_backup_name}}")${_option:${#_backup_name}}
    fi

    echo -n "${_option}"
}

function _fdm::all_backups_screen::make_anonymous_backup_option() {
    util::mandatory_arg_count 1 "$@"
    local readonly _backup="$1"
    local readonly _backup_name=$(basename "${_backup}")

    local _option
    _option="${_backup_name} ($(util::totalsize_human_readable "${_backup}"))"
    _option=$(display::trim_column "${_option}" "${_terminator}")
    if [[ "${#_option}" -le "${#_backup_name}" ]]; then
        # do not trim to less than the backup name; we will need it in full to enter the backup screen
        _option=$(fdm::theme::anonymous "${_backup_name}")
    else
        # apply theme to the backup name only
        _option=$(fdm::theme::anonymous "${_option:0:${#_backup_name}}")${_option:${#_backup_name}}
    fi

    echo -n "${_option}"
}

function _fdm::all_backups_screen::refresh_action_list() {
    all_backups_screen_data__options+=("Create [a]nonymous backup"); all_backups_screen_data__option_export="${all_backups_screen_data__options[-1]}"
    all_backups_screen_data__options+=("Create [p]ersistent backup"); all_backups_screen_data__option_export_persistent="${all_backups_screen_data__options[-1]}"

    all_backups_screen_data__options+=("[R]efresh screen"); all_backups_screen_data__option_refresh="${all_backups_screen_data__options[-1]}"
    all_backups_screen_data__options+=("[B]ack"); all_backups_screen_data__option_back="${all_backups_screen_data__options[-1]}"
    all_backups_screen_data__options+=("[Q]uit"); all_backups_screen_data__option_quit="${all_backups_screen_data__options[-1]}"
}

function fdm::all_backups_screen::header() {
    fdm::message::introduce_current_screen
    echo
    echo -n "Found $(fdm::theme::dynamic "${all_backups_screen_data__backups_count}") backup(s)"

    if [[ "${all_backups_screen_data__backups_count}" -ne 0 ]]; then

        echo -n " taking up $(fdm::theme::dynamic "${all_backups_screen_data__total_size}") of disk space: "

        if [[ "${all_backups_screen_data__persistent_count}" -eq 1 ]]; then
            echo -n "$(fdm::theme::dynamic "${all_backups_screen_data__persistent_count}") backup is $(fdm::theme::persistent 'persistent'), "
        else
            echo -n "$(fdm::theme::dynamic "${all_backups_screen_data__persistent_count}") backups are $(fdm::theme::persistent 'persistent'), "
        fi

        if [[ "${all_backups_screen_data__anonymous_count}" -eq 1 ]]; then
            echo -n "$(fdm::theme::dynamic "${all_backups_screen_data__anonymous_count}") backup is $(fdm::theme::anonymous 'anonymous')"
        else
            echo -n "$(fdm::theme::dynamic "${all_backups_screen_data__anonymous_count}") backups are $(fdm::theme::anonymous 'anonymous')"
        fi
        echo
   else
        echo
    fi

    echo
    echo "This service is configured to keep at most $(fdm::theme::dynamic "$(docker::service_anonymous_backup_history_size "${FDM_SERVICE}")") anonymous backup(s)."
}

function fdm::all_backups_screen::menu() {
    if [[ "${all_backups_screen_data__backups_count}" -ne 0 ]]; then
        echo "To enter the $(fdm::message::screen_name 'backup_screen'), type the backup number ($(fdm::theme::dynamic "1..${all_backups_screen_data__backups_count}")) at the prompt."
        echo
    fi

    fdm::message::how_to_execute_operations "$(( all_backups_screen_data__backups_count+1 ))" "${#all_backups_screen_data__options[@]}"
    echo

    local readonly _choice=$(prompt::choice "${all_backups_screen_data__options[@]}")
    case "${_choice}" in
        "${all_backups_screen_data__option_export}" )
            echo "Creating anonymous backup of '${FDM_SERVICE}' data..."
            fdm::run::confirmed_script 'fdm::all_backups_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/export_service_data" "${FDM_SERVICE}"
            ;;
        "${all_backups_screen_data__option_export_persistent}" )
            echo "Creating persistent backup of '${FDM_SERVICE}' data..."

            local _description=$(prompt::input_not_empty 'Please input a description for this backup')
            _description=${_description//\"/\\\"}

            fdm::run::confirmed_script 'fdm::all_backups_screen::validate' "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/bin/export_service_data" "${FDM_SERVICE}" "\"${_description}\""
            ;;
        "${all_backups_screen_data__option_refresh}" )
            ;;
        "${all_backups_screen_data__option_back}" )
            # shellcheck disable=SC2034
            FDM_SCREEN='service_screen'
            ;;
        "${all_backups_screen_data__option_quit}" )
            exit 0
            ;;
        *)
            if [[ -n "${_choice}" ]]; then
                _choice=$(text::remove_format_sequences "${_choice}")
                local readonly _backup="$(echo -n "$(docker::container_backups_root "${FDM_SERVICE}")/${_choice}" | cut -d ' ' -f 1)"
                # shellcheck disable=SC2034
                FDM_SCREEN='backup_screen'
                # shellcheck disable=SC2034
                FDM_BACKUP="${_backup}"
            fi
            ;;
    esac
}

function fdm::all_backups_screen::validate() {
    fdm::validation::check_conditions_for_all_backups_screen

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        # shellcheck disable=SC2034
        FDM_SCREEN='service_screen'
        return 1
    fi

    return 0
}

