# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlydocker

[[ -z "${_fdm_screen_sh_included}" ]] && _fdm_screen_sh_included='yes' || return

source "${FRIENDLY_BASH}"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/theme.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/properties.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/display.sh"

source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/screens/main_screen.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/screens/service_screen.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/screens/all_backups_screen.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/screens/backup_screen.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/screens/config_files_screen.sh"
source "${FRIENDLY_DOCKER_SCRIPTS_ROOT}/manager/screens/invalid_config_files_screen.sh"

function fdm::screen::starting() {
    _fdm::screen::init
    echo "$(fdm::theme::application "${FDM_APPLICATION_NAME}") is starting, please be patient..."
    fdm::display::horizontal_line
}

function fdm::screen::display_current() {
    util::recommended_arg_count 0 "$@"
    local readonly _screen="${FDM_SCREEN}"

    if [[ -n "${FDM_ERROR_REASON}" ]]; then
        eval "fdm::${_screen}::validate" || return
    else
        if ! eval "fdm::${_screen}::validate"; then
            fdm::screen::error
            return
        fi
    fi
    
    eval "fdm::${_screen}::refresh_data"

    _fdm::screen::init
    eval "fdm::${_screen}::header"
    fdm::display::horizontal_line
    eval "fdm::${_screen}::menu"

    [[ -n "${FDM_ERROR_REASON}" ]] && fdm::screen::error
}

function fdm::screen::error() {
    [[ -z ${FDM_SCREEN} ]] && return

    _fdm::screen::init
    echo "$(fdm::theme::application "${FDM_APPLICATION_NAME}") has detected a problem!"
    fdm::theme::invalid "${FDM_ERROR_REASON}"; echo

    fdm::display::horizontal_line

    echo "$(fdm::theme::application "${FDM_APPLICATION_NAME}") will now try to go back the closest valid screen."
    echo
    prompt::press_to_continue
}

function fdm::screen::fatal_error() {
    _fdm::screen::init
    echo "$(fdm::theme::application "${FDM_APPLICATION_NAME}") has detected a problem!" 
    fdm::theme::invalid "${FDM_ERROR_REASON}"; echo

    fdm::display::horizontal_line

    echo "This is a fatal error. You will not be able to use $(fdm::theme::application "${FDM_APPLICATION_NAME}") until the problem is fixed."
    echo
    prompt::press_to_continue
    exit 1
}

function _fdm::screen::init() {
    clear
    fdm::display::title_bar
    echo
}

